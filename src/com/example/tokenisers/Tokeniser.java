/**
 * 
 */
package com.example.tokenisers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import com.example.interfaces.TokeniserInterface;

/**
 * @author Vanessa_CU
 *
 */
public class Tokeniser implements TokeniserInterface {
	private URL url;
	private HashMap map = new HashMap<String, Integer>();
	private int wordTotal;
	
	public static void main( String[] args ){
		Tokeniser tokeniser;
		try {
			tokeniser = new Tokeniser(
					new URL("http://www.textfiles.com/stories/beggars.txt"));
			tokeniser.buildTokenMap();
			tokeniser.getWordTotal();
			System.out.println("i "+ tokeniser.countWordFrequency("i"));
			System.out.println("a "+ tokeniser.countWordFrequency("a"));
			System.out.println("and "+ tokeniser.countWordFrequency("and"));
			System.out.println("the "+ tokeniser.countWordFrequency("the"));
			tokeniser.countDistinctWords();
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public Tokeniser(URL url) {
		if( null == url || "".equals(url) ) {
			System.out.println("Please provide a url");
		} else {
			this.url = url;
		}
	}
	
	@Override
	public HashMap buildTokenMap() throws IOException {

		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(this.url.openStream()));
		
		StringBuilder builder = new StringBuilder();
		String line = bufferedReader.readLine();
		while( null != line ) {
			builder.append(line);
			line = bufferedReader.readLine();
		}
		bufferedReader.close();

		final String allText = builder.toString();
		
		wordTotal = 0;
		String delim = "?!-= \n\r\t.,:;()[]{}&*^%$�"; //insert here all delimitators
		StringTokenizer st = new StringTokenizer(allText,delim);
		
		while (st.hasMoreTokens()) {
			String word = st.nextToken().toLowerCase();
		    if( !map.containsKey(word)) {
				map.put(word, 1);
				System.out.println("NEW WORD: " + word);
				wordTotal += 1;
			} else {
				wordTotal += 1;
				Integer count = (Integer) map.get(word);
				map.remove(word);
				map.put(word, ++count);
//				System.out.println(word.toUpperCase() + " COUNT: "+map.get(word));
			}
		}
		return map;
	}

	public int getWordTotal() {
		System.out.println("TOTAL "+ wordTotal);
		return this.wordTotal;
	}
	
	/* (non-Javadoc)
	 * @see com.example.interfaces.TokeniserInterface#countWordFrequency(java.lang.String)
	 */
	@Override
	public int countWordFrequency(String word) {
		return (map.containsKey(word)) ? (Integer) map.get(word) : 0;
	}
	
	public void sortMapByMostCommonWord() {
		//TODO
	}

	/* (non-Javadoc)
	 * @see com.example.interfaces.TokeniserInterface#countDistinctWords()
	 */
	@Override
	public int countDistinctWords() {
		System.out.println(map.size());
		return map.size();
	}
	
	/* (non-Javadoc)
	 * @see com.example.interfaces.TokeniserInterface#findMostCommonWord()
	 */
	@Override
	public ArrayList<String> findMostCommonWords() {
		ArrayList<String> mostCommonWords = new ArrayList<String>();
		int counter = 0;
		
		for( Object key : map.keySet() ) {
			Integer wordCount = (Integer) map.get(key);
			if( wordCount > counter ) {
				mostCommonWords.clear();
				mostCommonWords.add((String) key);
			} else if (wordCount == counter) {
				mostCommonWords.add((String) key);
			}
		}
		return mostCommonWords;
	}

	public void sortWordMapByCount() {
		//TODO
	}
	
	/* (non-Javadoc)
	 * @see com.example.interfaces.TokeniserInterface#findNthMostCommonWord()
	 */
	@Override
	public String findNthMostCommonWord() {
		// TODO Auto-generated method stub
		return null;
	}

}
