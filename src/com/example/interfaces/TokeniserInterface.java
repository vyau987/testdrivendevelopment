/**
 * 
 */
package com.example.interfaces;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Vanessa_CU
 *
 */
public interface TokeniserInterface {
	public HashMap buildTokenMap() throws IOException;
	public int getWordTotal();
	public int countWordFrequency(String word);
	public int countDistinctWords();
	public ArrayList<String> findMostCommonWords();
	public String findNthMostCommonWord();
}
