/**
 * 
 */
package com.example.test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;

import com.example.tokenisers.Tokeniser;

/**
 * @author Vanessa_CU
 *
 */
public class TokeniserTest {
	private URL url;
	private Tokeniser tokeniser;
	private HashMap tokenMap;
	
	public TokeniserTest() throws IOException {
		this.url = new URL("http://www.textfiles.com/stories/beggars.txt");
		this.tokeniser = new Tokeniser(url);
		this.tokeniser.buildTokenMap();
	}
	
	@Test
	public void testBuildTokenMap() throws IOException {
		this.tokenMap = this.tokeniser.buildTokenMap();
	}
	
	@Test
	public void testCountAllWords(){
		//beggars
		Assert.assertEquals(18176, this.tokeniser.getWordTotal());
	}
	
	@Test
	public void testCountWordFrequency() {
		//beggars
		Assert.assertEquals(0, tokeniser.countWordFrequency(""));
		Assert.assertEquals(398, tokeniser.countWordFrequency("and"));
	}
	
	@Test
	public void testCountDistinctWords() {
		//beggars
		Assert.assertEquals(18176, tokeniser.countDistinctWords());
	}
	
	@Test
	public void testFindMostCommonWord() {
		//TODO
	}
	
	@Test
	public void testFindNthMostCommonWord() {
		//TODO
	}
}
